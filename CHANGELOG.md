## Version 2.0.0

1.适配DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
2.包管理工具由npm切换成ohpm

## v1.1.0
1. 名称由LiveEventBus-ETS修改LiveEventBus。
2. 旧的包@ohos/LiveEventBus-ETS已不维护，请使用新包@ohos/LiveEventBus

## v1.0.1

- api8升级到api9

## v1.0.0

1. 支持采用观察者模式实现事件总线
2. 支持不定义消息直接发送和先定义消息再发送
3. 支持进程内消息发送
4. 支持App内发送消息，跨进程使用
5. 支持App之间发送消息
6. 支持延迟发送消息
7. 支持Sticky粘性消息
8. 支持生命周期感知能力